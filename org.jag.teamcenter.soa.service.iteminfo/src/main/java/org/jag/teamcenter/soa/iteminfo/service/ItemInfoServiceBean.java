/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.iteminfo.service;

import org.jag.teamcenter.soa.iteminfo.model.Group;
import org.jag.teamcenter.soa.iteminfo.model.ItemInfo;
import org.jag.teamcenter.soa.iteminfo.model.User;
import org.jag.teamcenter.soa.services.GroupBean;
import org.jag.teamcenter.soa.services.GroupConnector;
import org.jag.teamcenter.soa.services.ItemBean;
import org.jag.teamcenter.soa.services.ItemConnector;
import org.jag.teamcenter.soa.services.UserBean;
import org.jag.teamcenter.soa.services.UserConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * @author José A. García Sánchez
 */
class ItemInfoServiceBean implements ItemInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemInfoServiceBean.class);

    private final ItemConnector itemConnector;
    private final UserConnector userConnector;
    private final GroupConnector groupConnector;

    @Inject
    ItemInfoServiceBean(final ItemConnector itemConnector, final UserConnector userConnector,
            final GroupConnector groupConnector) {
        this.itemConnector = itemConnector;
        this.userConnector = userConnector;
        this.groupConnector = groupConnector;
    }

    @Override
    public ItemInfo find(final String itemId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("find({})", itemId);
        }

        final ItemBean item = itemConnector.findItemById(itemId);
        if (item == null) {
            return null;
        }
        final UserBean owningUser = userConnector.getOwningUser(item);
        final GroupBean owningGroup = groupConnector.getOwningGroup(item);

        final User user = new User();
        user.setId(owningUser.getId());
        user.setName(owningUser.getName());

        final Group group = new Group();
        if (owningGroup != null) {
            group.setName(owningGroup.getName());
        } else {
            group.setName("");
        }

        final ItemInfoBean.Builder builder = new ItemInfoBean.Builder(item.getId());
        builder.withName(item.getName());
        builder.withType(item.getType());
        builder.withNumRevisions(item.getNumRevisions());
        builder.withUser(user);
        builder.withGroup(group);

        return builder.build();
    }
}
