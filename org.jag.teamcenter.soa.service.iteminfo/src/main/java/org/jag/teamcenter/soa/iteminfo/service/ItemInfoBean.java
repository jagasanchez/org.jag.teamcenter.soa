/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.iteminfo.service;

import org.jag.teamcenter.soa.iteminfo.model.Group;
import org.jag.teamcenter.soa.iteminfo.model.ItemInfo;
import org.jag.teamcenter.soa.iteminfo.model.User;

/**
 * @author José A. García Sánchez
 */
class ItemInfoBean implements ItemInfo {
    private final String id;
    private final String name;
    private final String type;
    private final int numRevisions;
    private final User user;
    private final Group group;

    private ItemInfoBean(final Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.type = builder.type;
        this.numRevisions = builder.numRevisions;
        this.user = builder.user;
        this.group = builder.group;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getNumRevisions() {
        return numRevisions;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public Group getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return String.format("%s{id: [%s], name: [%s], type: [%s], numRevisions: [%d], user: [%s], group: [%s]}",
                getClass().getSimpleName(), id, name, type, numRevisions, user, group);
    }

    public static class Builder {
        private final String id;
        private String name = "";
        private String type = "";
        private int numRevisions = -1;
        private User user;
        private Group group;

        public Builder(final String id) {
            this.id = id;
        }

        public Builder withName(final String name) {
            this.name = name;
            return this;
        }

        public Builder withType(final String type) {
            this.type = type;
            return this;
        }

        public Builder withNumRevisions(int numRevisions) {
            this.numRevisions = numRevisions;
            return this;
        }

        public Builder withUser(final User user) {
            this.user = user;
            return this;
        }

        public Builder withGroup(final Group group) {
            this.group = group;
            return this;
        }

        public ItemInfoBean build() {
            return new ItemInfoBean(this);
        }
    }
}
