/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.iteminfo.service;

import static com.google.common.truth.Truth.assertThat;

import org.jag.teamcenter.soa.iteminfo.model.Group;
import org.jag.teamcenter.soa.iteminfo.model.User;
import org.junit.Before;
import org.junit.Test;

/**
 * @author José A. García Sánchez
 */
public class ItemInfoBeanTest {
    private ItemInfoBean.Builder underTestBuilder;

    @Before
    public void setUp() throws Exception {
        underTestBuilder = new ItemInfoBean.Builder("012345");
    }

    @Test
    public void createWithMinimalAttributes() {
        final ItemInfoBean itemInfo = underTestBuilder.build();

        assertThat(itemInfo).isNotNull();
        assertThat(itemInfo.getId()).isEqualTo("012345");
        assertThat(itemInfo.getName()).isEmpty();
        assertThat(itemInfo.getType()).isEmpty();
        assertThat(itemInfo.getNumRevisions()).isEqualTo(-1);
        assertThat(itemInfo.getUser()).isNull();
        assertThat(itemInfo.getGroup()).isNull();
        assertThat(itemInfo.toString()).isEqualTo(
                "ItemInfoBean{id: [012345], name: [], type: [], numRevisions: [-1], user: [null], group: [null]}");
    }

    @Test
    public void createWithAllAttributes() {
        final User user = new User();
        user.setId("userId");
        user.setName("user name");

        final Group group = new Group();
        group.setName("group name");

        underTestBuilder.withName("item name").withType("item type").withNumRevisions(2).withUser(user)
                .withGroup(group);
        final ItemInfoBean itemInfo = underTestBuilder.build();

        assertThat(itemInfo).isNotNull();
        assertThat(itemInfo.getId()).isEqualTo("012345");
        assertThat(itemInfo.getName()).isEqualTo("item name");
        assertThat(itemInfo.getType()).isEqualTo("item type");
        assertThat(itemInfo.getNumRevisions()).isEqualTo(2);
        assertThat(itemInfo.getUser()).isNotNull();
        assertThat(itemInfo.getUser().getId()).isEqualTo("userId");
        assertThat(itemInfo.getUser().getName()).isEqualTo("user name");
        assertThat(itemInfo.getGroup()).isNotNull();
        assertThat(itemInfo.getGroup().getName()).isEqualTo("group name");
        assertThat(itemInfo.toString()).isEqualTo(
                "ItemInfoBean{id: [012345], name: [item name], type: [item type], numRevisions: [2], user: [User{id: [userId], name: [user name]}], group: [Group{name: [group name]}]}");
    }
}
