/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

/**
 * @author José A. García Sánchez
 *
 */
abstract class AbstractItemBean implements ItemBean {
    private String id = "";
    private String name = "";
    private String type = "";
    private int numRevisions = -1;

    @Override
    public final String getId() {
        return id;
    }

    @Override
    public final void setId(final String id) {
        this.id = id;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final void setName(final String name) {
        this.name = name;
    }

    @Override
    public final String getType() {
        return type;
    }

    @Override
    public final void setType(final String type) {
        this.type = type;
    }

    @Override
    public final int getNumRevisions() {
        return numRevisions;
    }

    @Override
    public final void setNumRevisions(final int numRevisions) {
        this.numRevisions = numRevisions;
    }

    @Override
    public final String toString() {
        return String.format("%s{id: [%s], name: [%s], type: [%s], numRevisions: [%d]}", getClass().getSimpleName(), id,
                name, type, numRevisions);
    }
}
