# EasyTCSOA
This is a collection of libraries to lighten the connection to Teamcenter using
SOA and make easier implementations of SOA services.

## Layers
There are at least there layers within the repository (from bottom to top):

* core
* connector (`core.connector`)
* service

## _loose_ and _strong_ libraries
Because Siemens is delivering two set of java libraries to connect to
Teamcenter, _loose_ and _strong_, there are also two sets of libraries
depending on those. The functionality remains the same, it's only about
internal dependencies to the Siemens libraries.

Hence one can find, for instance, for **core** layer the following libraries

1. **core** layer:

	* `core.common`: common functionality used by both
	_loose_ and _strong_ implementations 
	* `core` (loose)
	* `strong.core` (strong)

1. **core connector** layer:

	* `core.connector.common`: common functionality used by
	both _loose_ and _strong_ implementations
	* `core.connector` (loose)
	* `strong.core.connector` (strong)

The dependencies to Siemens libraries don't go further than this layer. Above
this, only business objects and logic for the customized application must be
developed, without any reference to Teamcenter's libraries.

## Creating instances of Business Objects
`EasyTCSOA` has been build using dependency injection with
[`guice`](https://github.com/google/guice).

To get the business service, and some business objects, instances, instead
of creating using `new`, one must create an instance of `guice` _injector_:

		final Injector injector = Guice.createInjector(new ServicesModule(), new ConnectorModule());

and then using this `injector` to get the instances of the objects and
services we are interested in:

		final ConnectionConfigurationFactory connectionConfigurationFactory =
			injector.getInstance(ConnectionConfigurationFactory.class);
		final ConnectionConnector connectionConnector = injector.getInstance(ConnectionConnector.class);
		final ItemInfoService itemInfoService = injector.getInstance(ItemInfoService.class)


## Connecting to Teamcenter using SOA
To connect to Teamcenter using SOA, first a `ConnectionConfiguration`,
containing the host and a discriminator, must be created. To do that, use
`ConnectionConfigurationFactory`.

The `ConnectionConfiguration` instance, along with an instance of
`Credentials` are passed to a `ConnectionConnector` instance
to connect to the host using the method `connect()`. Later using `login()`
and `logout()` from `ConnectionConnector` to log in/out to/from
Teamcenter.

		final String host = http://localhost:8080/tc
		final String discriminator = "MyAppDiscriminator";
		
		// Create a ConnectionConfiguration
		final ConnectionConfiguration connectionConfiguration =
			connectionConfigurationFactory.createConnection(host, discriminator);
		
		// Credentials
		final Credentials credentials = new Credentials() {
			public String getUsername() { return "userId"; }
			public String getPassword() { return "password"; }
			public String getGroup() { return ""; }
			public String getRole() { return ""; }
		};
		
		// Connect to the host
		connectionConnector.connect(connectionConfiguration, credentials);
		
		// Log in
		connectionConnector.login();
		
		// ... call some EasyTCSOA service:
		// final ItemInfo item = itemInfoService.find("000016");
		
		// Log out
		connectionConnector.logout();
 

## EasyTCSOA and Teamcenter RAC
If there is the need to develop a client SOA functionality within Teamcenter RAC, instead of creating from scratch a SOA connection,
it can be used a SOA connection from the RAC session:

		final Connection soaConnection = tcSession.getSoaConnection();

Instead of using `ConnectionConfigurationFactory` and `ConnectionConnector`,
`RacConnectionConnector` can be used instead. This class has the only method
`connect(connection:Connection, credentials:Credentials)`:

		final Injector injector = Guice.createInjector(new ConnectorModule());
		final RacConnectionConnector connectionConnector = injector.getInstance(RacConnectionConnector.class);
		
		// ...
		final Connection soaConnection = tcSession.getSoaConnection();
		final Credentials credentials = new Credentials() {
			public String getUsername() { return "userId"; }
			public String getPassword() { return "password"; }
			public String getGroup() { return ""; }
			public String getRole() { return ""; }
		};
		
		// Connects to Teamcenter through SOA using the connection from RAC
		connectionConnector.connect(soaConnection, credentials);

## Example: ItemInfo
As an example of a client implementation, there is a sub-module `org.jag.teamcenter.soa.client.commandline`
that gets some information about an item identified with its `id`.

		$> java -jar client.commandline-v1.0_11000.2.0.jar
			-host http://localhost:8080/tc
			-u infodba -p infodba -id 000242
		...
		[main] INFO org.jag.teamcenter.client.Main - item: ItemInfoBean{id: [000242], name: [000242], type: [Item], numRevisions: [4], user: [User{id: [infodba], name: [infodba]}], group: [Group{name: [dba]}]}
		...