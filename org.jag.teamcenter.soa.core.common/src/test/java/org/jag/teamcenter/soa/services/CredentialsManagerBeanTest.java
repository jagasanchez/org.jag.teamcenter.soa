/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.teamcenter.schemas.soa._2006_03.exceptions.InvalidCredentialsException;
import com.teamcenter.schemas.soa._2006_03.exceptions.InvalidUserException;
import com.teamcenter.soa.client.CredentialManager;
import com.teamcenter.soa.exceptions.CanceledOperationException;

/**
 * @author José A. García Sánchez
 */
public class CredentialsManagerBeanTest {
    private CredentialsManagerBean underTest;

    private CredentialsBean credentials;

    private String discriminator;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        credentials = new CredentialsBean();
        credentials.setUsername("userId");
        credentials.setPassword("password");
        credentials.setGroup("group");
        credentials.setRole("role");
        discriminator = "myDiscriminator";

        underTest = new CredentialsManagerBean(credentials, discriminator);
    }

    @Test
    public void getCredentialType() {
        assertThat(underTest.getCredentialType()).isEqualTo(CredentialManager.CLIENT_CREDENTIAL_TYPE_STD);

        underTest.setCredentialType(CredentialManager.CLIENT_CREDENTIAL_TYPE_SSO);
        assertThat(underTest.getCredentialType()).isEqualTo(CredentialManager.CLIENT_CREDENTIAL_TYPE_SSO);
    }

    @Test
    public void getCredentialsByInvalidCredentialsException() throws CanceledOperationException {
        final InvalidCredentialsException exception = new InvalidCredentialsException();
        final String[] credentialsArray = underTest.getCredentials(exception);

        assertThat(credentialsArray).isNotNull();
        assertThat(credentialsArray).isNotEmpty();
        assertThat(credentialsArray).hasLength(5);
        assertThat(credentialsArray)
                .isEqualTo(new String[] { "userId", "password", "group", "role", "myDiscriminator" });
    }

    @Test
    public void getCredentialsByInvalidUserException() throws CanceledOperationException {
        final InvalidUserException exception = new InvalidUserException();
        final String[] credentialsArray = underTest.getCredentials(exception);

        assertThat(credentialsArray).isNotNull();
        assertThat(credentialsArray).isNotEmpty();
        assertThat(credentialsArray).hasLength(5);
        assertThat(credentialsArray)
                .isEqualTo(new String[] { "userId", "password", "group", "role", "myDiscriminator" });
    }

    @Test
    @Ignore("To be adapted")
    public void setGroupRole() throws CanceledOperationException {
        underTest.setGroupRole("new group", "new role");

        assertThat(credentials.getUsername()).isEqualTo("userId");
        assertThat(credentials.getPassword()).isEqualTo("password");
        assertThat(credentials.getGroup()).isEqualTo("new group");
        assertThat(credentials.getRole()).isEqualTo("new role");

        final InvalidUserException exception = new InvalidUserException();
        final String[] credentialsArray = underTest.getCredentials(exception);

        assertThat(credentialsArray).isNotNull();
        assertThat(credentialsArray).isNotEmpty();
        assertThat(credentialsArray).hasLength(5);
        assertThat(credentialsArray)
                .isEqualTo(new String[] { "userId", "password", "new group", "new role", "myDiscriminator" });
    }

    @Test
    @Ignore("To be adapted")
    public void setUserPasswordGroupRole() throws CanceledOperationException {
        underTest.setUserPassword("newUserId", "new password", "new discriminator");

        assertThat(credentials.getUsername()).isEqualTo("newUserId");
        assertThat(credentials.getPassword()).isEqualTo("new password");
        assertThat(credentials.getGroup()).isEqualTo("group");
        assertThat(credentials.getRole()).isEqualTo("role");

        final InvalidUserException exception = new InvalidUserException();
        final String[] credentialsArray = underTest.getCredentials(exception);

        assertThat(credentialsArray).isNotNull();
        assertThat(credentialsArray).isNotEmpty();
        assertThat(credentialsArray).hasLength(5);
        assertThat(credentialsArray)
                .isEqualTo(new String[] { "newUserId", "new password", "group", "role", "new discriminator" });
    }
}
