/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import static com.google.common.truth.Truth.assertThat;

import org.jag.teamcenter.soa.model.Protocol;
import org.jag.teamcenter.soa.services.ConnectionConfigurationHttpBean;
import org.junit.Before;
import org.junit.Test;

/**
 * @author José A. García Sánchez
 */
public class ConnectionConfigurationHttpBeanTest {
    private ConnectionConfigurationHttpBean underTest;

    @Before
    public void setUp() throws Exception {
        underTest = new ConnectionConfigurationHttpBean("https://localhost:8080/tc", "MyDiscriminator");
    }

    @Test
    public void getHost() {
        assertThat(underTest.getHost()).isEqualTo("https://localhost:8080/tc");
    }

    @Test
    public void getProtocol() {
        assertThat(underTest.getProtocol()).isEqualTo(Protocol.HTTP);
    }

    @Test
    public void getDiscriminator() {
        assertThat(underTest.getDiscriminator()).isEqualTo("MyDiscriminator");
    }
}
