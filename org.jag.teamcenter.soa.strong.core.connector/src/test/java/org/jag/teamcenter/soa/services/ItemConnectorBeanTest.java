/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.allOf;
import static org.jag.hamcrest.matchers.ArrayStringMatcher.arrayHasSize;
import static org.jag.hamcrest.matchers.ArrayStringMatcher.stringArrayContaining;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jag.hamcrest.matchers.GetItemFromAttributeInfoMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.teamcenter.services.strong.core.DataManagementService;
import com.teamcenter.services.strong.core._2007_01.DataManagement.GetItemFromIdPref;
import com.teamcenter.services.strong.core._2009_10.DataManagement;
import com.teamcenter.services.strong.core._2009_10.DataManagement.GetItemFromAttributeInfo;
import com.teamcenter.services.strong.core._2009_10.DataManagement.GetItemFromAttributeResponse;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.strong.Item;
import com.teamcenter.soa.exceptions.NotLoadedException;

/**
 * @author José A. García Sánchez
 */
public class ItemConnectorBeanTest {
    @InjectMocks
    private ItemConnectorBean underTest;

    @Mock
    private DataManagementServiceProviderBean dataManagementServiceProviderMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findItemById() throws NotLoadedException {
        final DataManagementService service = mock(DataManagementService.class);
        when(dataManagementServiceProviderMock.getService()).thenReturn(service);

        final GetItemFromAttributeResponse response = new GetItemFromAttributeResponse();
        response.output = new DataManagement.GetItemFromAttributeItemOutput[] {
                new DataManagement.GetItemFromAttributeItemOutput() };

        final Item tcItem = mock(Item.class);
        when(tcItem.get_object_name()).thenReturn("Item test name");
        when(tcItem.get_object_type()).thenReturn("Excel Template");
        when(tcItem.get_revision_list()).thenReturn(new ModelObject[12]);
        response.output[0].item = tcItem;

        when(service.getItemFromAttribute(any(GetItemFromAttributeInfo[].class), anyInt(),
                any(GetItemFromIdPref.class))).thenReturn(response);

        final AbstractItemBean item = underTest.findItemById("000012");

        assertThat(item.getId()).isEqualTo("000012");
        assertThat(item.getName()).isEqualTo("Item test name");
        assertThat(item.getType()).isEqualTo("Excel Template");
        assertThat(item.getNumRevisions()).isEqualTo(12);

        verify(service).getItemFromAttribute(argThat(GetItemFromAttributeInfoMatcher.arrayHasSize(1)), eq(1),
                any(GetItemFromIdPref.class));
        verify(service).getProperties(any(ModelObject[].class),
                argThat(allOf(arrayHasSize(3), stringArrayContaining("object_name", "revision_list", "object_type"))));
    }

    @Test
    public void findItemByIdObjectNameNotLoaded() throws NotLoadedException {
        final DataManagementService service = mock(DataManagementService.class);
        when(dataManagementServiceProviderMock.getService()).thenReturn(service);

        final GetItemFromAttributeResponse response = new GetItemFromAttributeResponse();
        response.output = new DataManagement.GetItemFromAttributeItemOutput[] {
                new DataManagement.GetItemFromAttributeItemOutput() };

        final Item tcItem = mock(Item.class);
        when(tcItem.get_object_name()).thenThrow(new NotLoadedException("Fake exception"));
        when(tcItem.get_object_type()).thenReturn("Excel Template");
        when(tcItem.get_revision_list()).thenReturn(new ModelObject[12]);
        response.output[0].item = tcItem;

        when(service.getItemFromAttribute(any(GetItemFromAttributeInfo[].class), anyInt(),
                any(GetItemFromIdPref.class))).thenReturn(response);

        final AbstractItemBean item = underTest.findItemById("000012");

        assertThat(item.getId()).isEqualTo("000012");
        assertThat(item.getName()).isEqualTo("");
        assertThat(item.getType()).isEqualTo("Excel Template");
        assertThat(item.getNumRevisions()).isEqualTo(12);
    }

    @Test
    public void findItemByIdObjectTypeNotLoaded() throws NotLoadedException {
        final DataManagementService service = mock(DataManagementService.class);
        when(dataManagementServiceProviderMock.getService()).thenReturn(service);

        final GetItemFromAttributeResponse response = new GetItemFromAttributeResponse();
        response.output = new DataManagement.GetItemFromAttributeItemOutput[] {
                new DataManagement.GetItemFromAttributeItemOutput() };

        final Item tcItem = mock(Item.class);
        when(tcItem.get_object_name()).thenReturn("Item test name");
        when(tcItem.get_object_type()).thenThrow(new NotLoadedException("Fake exception"));
        when(tcItem.get_revision_list()).thenReturn(new ModelObject[12]);
        response.output[0].item = tcItem;

        when(service.getItemFromAttribute(any(GetItemFromAttributeInfo[].class), anyInt(),
                any(GetItemFromIdPref.class))).thenReturn(response);

        final AbstractItemBean item = underTest.findItemById("000012");

        assertThat(item.getId()).isEqualTo("000012");
        assertThat(item.getName()).isEqualTo("Item test name");
        assertThat(item.getType()).isEqualTo("");
        assertThat(item.getNumRevisions()).isEqualTo(12);
    }

    @Test
    public void findItemByIdRevisionListNotLoaded() throws NotLoadedException {
        final DataManagementService service = mock(DataManagementService.class);
        when(dataManagementServiceProviderMock.getService()).thenReturn(service);

        final GetItemFromAttributeResponse response = new GetItemFromAttributeResponse();
        response.output = new DataManagement.GetItemFromAttributeItemOutput[] {
                new DataManagement.GetItemFromAttributeItemOutput() };

        final Item tcItem = mock(Item.class);
        when(tcItem.get_object_name()).thenReturn("Item test name");
        when(tcItem.get_object_type()).thenReturn("Excel Template");
        when(tcItem.get_revision_list()).thenThrow(new NotLoadedException("Fake exception"));
        response.output[0].item = tcItem;

        when(service.getItemFromAttribute(any(GetItemFromAttributeInfo[].class), anyInt(),
                any(GetItemFromIdPref.class))).thenReturn(response);

        final AbstractItemBean item = underTest.findItemById("000012");

        assertThat(item.getId()).isEqualTo("000012");
        assertThat(item.getName()).isEqualTo("Item test name");
        assertThat(item.getType()).isEqualTo("Excel Template");
        assertThat(item.getNumRevisions()).isEqualTo(-1);

        verify(service).getItemFromAttribute(argThat(GetItemFromAttributeInfoMatcher.arrayHasSize(1)), eq(1),
                any(GetItemFromIdPref.class));
        verify(service).getProperties(any(ModelObject[].class),
                argThat(allOf(arrayHasSize(3), stringArrayContaining("object_name", "revision_list", "object_type"))));
    }

    @Test
    public void findItemByIdNotFound() {
        final DataManagementService service = mock(DataManagementService.class);
        when(dataManagementServiceProviderMock.getService()).thenReturn(service);

        final GetItemFromAttributeResponse response = new GetItemFromAttributeResponse();
        response.output = new DataManagement.GetItemFromAttributeItemOutput[0];

        when(service.getItemFromAttribute(any(GetItemFromAttributeInfo[].class), anyInt(),
                any(GetItemFromIdPref.class))).thenReturn(response);

        final AbstractItemBean item = underTest.findItemById("000020");

        assertThat(item).isNull();
    }
}
