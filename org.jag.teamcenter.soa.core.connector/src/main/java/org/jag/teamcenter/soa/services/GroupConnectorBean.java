/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.Property;
import com.teamcenter.soa.exceptions.NotLoadedException;

/**
 * @author José A. García Sánchez
 */
final class GroupConnectorBean implements GroupConnector {
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupConnectorBean.class);

    private DataManagementServiceProviderBean dataManagementServiceProvider;

    @Inject
    public GroupConnectorBean(final DataManagementServiceProviderBean dataManagementServiceProviderBean) {
        this.dataManagementServiceProvider = dataManagementServiceProviderBean;
    }

    @Override
    public GroupBean getOwningGroup(final ItemBean item) {

        try {
            final Property propertyObject = ((ItemBeanImpl) item).getItem().getPropertyObject("owning_group");
            final ModelObject group = (ModelObject) propertyObject.getModelObjectValue();
            dataManagementServiceProvider.getService().getProperties(new ModelObject[] { group },
                    new String[] { "name" });

            final GroupBeanImpl groupBean = new GroupBeanImpl();
            groupBean.setGroup(group);
            groupBean.setName(group.getPropertyDisplayableValue("name"));

            return groupBean;
        } catch (NotLoadedException e) {
            LOGGER.error("Not loaded exception", e);
        }
        return null;
    }
}
