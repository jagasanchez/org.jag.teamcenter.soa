/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.soa.services;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.teamcenter.soa.client.model.ModelObject;
import com.teamcenter.soa.client.model.Property;
import com.teamcenter.soa.exceptions.NotLoadedException;

/**
 * @author José A. García Sánchez
 */
public class UserConnectorBeanTest {
    @InjectMocks
    private UserConnectorBean underTest;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getOwningUser() throws NotLoadedException {
        final ModelObject tcUser = mock(ModelObject.class);
        when(tcUser.getPropertyDisplayableValue("user_id")).thenReturn("user id");
        when(tcUser.getPropertyDisplayableValue("user_name")).thenReturn("user name");
        final ModelObject tcItem = mock(ModelObject.class);
        final Property owningUserProperty = mock(Property.class);
        when(owningUserProperty.getModelObjectValue()).thenReturn(tcUser);
        when(tcItem.getPropertyObject("owning_user")).thenReturn(owningUserProperty);
        final ItemBeanImpl item = new ItemBeanImpl();
        item.setItem(tcItem);

        final UserBeanImpl user = (UserBeanImpl) underTest.getOwningUser(item);

        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo("user id");
        assertThat(user.getName()).isEqualTo("user name");
        assertThat(user.getUser()).isEqualTo(tcUser);
    }

    @Test
    public void getOwningUserNotLoadedException() throws NotLoadedException {
        final ModelObject tcUser = mock(ModelObject.class);
        when(tcUser.getPropertyDisplayableValue("user_id")).thenReturn("user id");
        when(tcUser.getPropertyDisplayableValue("user_name")).thenThrow(new NotLoadedException("Fake exception"));
        final ModelObject tcItem = mock(ModelObject.class);
        final Property owningUserProperty = mock(Property.class);
        when(owningUserProperty.getModelObjectValue()).thenReturn(tcUser);
        when(tcItem.getPropertyObject("owning_user")).thenReturn(owningUserProperty);
        final ItemBeanImpl item = new ItemBeanImpl();
        item.setItem(tcItem);

        final UserBeanImpl user = (UserBeanImpl) underTest.getOwningUser(item);

        assertThat(user).isNull();
    }
}
