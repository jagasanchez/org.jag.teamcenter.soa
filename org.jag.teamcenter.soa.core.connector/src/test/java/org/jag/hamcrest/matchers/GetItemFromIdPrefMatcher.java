/*
 * (c) 2018 - Jose Alberto Garcia Sanchez 
 */
package org.jag.hamcrest.matchers;

import org.hamcrest.Matcher;

import com.teamcenter.services.loose.core._2007_01.DataManagement.GetItemFromIdPref;

/**
 * @author Jose A. Garcia Sanchez
 */
public class GetItemFromIdPrefMatcher {

    public static Matcher<GetItemFromIdPref[]> arrayHasSize(int expected) {
        return new ArrayHasSize<>(expected);
    }
}
