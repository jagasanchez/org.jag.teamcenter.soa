/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.client.services;

import org.jag.teamcenter.client.model.BasicArguments;

import com.google.common.base.Strings;

/**
 * Implementation of {@link BasicArguments}.
 * 
 * @author José A. García Sánchez
 */
class BasicArgumentsBean implements BasicArguments {
    private final String username;
    private final String password;
    private final String group;
    private final String role;
    private final String host;

    protected BasicArgumentsBean(final Builder builder) {
        this.username = Strings.nullToEmpty(builder.username);
        this.password = Strings.nullToEmpty(builder.password);
        this.group = Strings.nullToEmpty(builder.group);
        this.role = Strings.nullToEmpty(builder.role);
        this.host = Strings.nullToEmpty(builder.host);
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getGroup() {
        return group;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public String getHost() {
        return host;
    }

    public static class Builder {
        private final String host;
        private final String username;
        private String password;
        private String group;
        private String role;

        public Builder(final String username, final String host) {
            this.username = username;
            this.host = host;
        }

        public Builder withPassword(final String password) {
            this.password = password;
            return this;
        }

        public Builder withGroup(final String group) {
            this.group = group;
            return this;
        }

        public Builder withRole(final String role) {
            this.role = role;
            return this;
        }

        public BasicArguments build() {
            return new BasicArgumentsBean(this);
        }
    }
}
