/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.client.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jag.teamcenter.client.model.Arguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

/**
 * @author José A. García Sánchez
 */
class ArgumentsServiceBean implements ArgumentsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArgumentsServiceBean.class);

    private static final String JAVA_JAR_ITEM_INFO = "java -jar itemInfo";

    private static final Option HOST_OPTION = Option.builder("host").hasArg().desc("Teamcenter instance/host").build();
    private static final Option USERNAME_OPTION = Option.builder("u").longOpt("username").hasArg()
            .desc("Teamcenter username").build();
    private static final Option PASSWORD_OPTION = Option.builder("p").longOpt("password").hasArg()
            .desc("Teamcenter user password").build();
    private static final Option GROUP_OPTION = Option.builder("g").longOpt("group").hasArg()
            .desc("Teamcenter user group")
            .build();
    private static final Option ROLE_OPTION = Option.builder("r").longOpt("role").hasArg().desc("Teamcenter user role")
            .build();
    private static final Option HELP_OPTION = Option.builder("h").longOpt("help").desc("help").build();
    private static final Option ITEM_ID_OPTION = Option.builder("id").longOpt("itemId").hasArg().desc("Item id")
            .build();

    private static final List<Option> MANDATORY_OPTIONS = new ArrayList<>();

    private final OptionsHelpFormatter optionsHelpFormatter;

    @Inject
    public ArgumentsServiceBean(final OptionsHelpFormatter optionsHelpFormatter) {
        this.optionsHelpFormatter = optionsHelpFormatter;

        MANDATORY_OPTIONS.add(HOST_OPTION);
        MANDATORY_OPTIONS.add(USERNAME_OPTION);
        MANDATORY_OPTIONS.add(ITEM_ID_OPTION);
    }

    @Override
    public Arguments parse(final String[] args) {
        final Options options = new Options();
        options.addOption(HOST_OPTION);
        options.addOption(USERNAME_OPTION);
        options.addOption(PASSWORD_OPTION);
        options.addOption(GROUP_OPTION);
        options.addOption(ROLE_OPTION);
        options.addOption(HELP_OPTION);
        options.addOption(ITEM_ID_OPTION);

        try {
            final CommandLine commandLine = new DefaultParser().parse(options, args);

            if (commandLine.hasOption(HELP_OPTION.getOpt())) {
                optionsHelpFormatter.printHelp(JAVA_JAR_ITEM_INFO, options);
                return null;
            }

            if (hasAllMandatoryOptions(commandLine)) {
                final BasicArgumentsBean.Builder builder = new BasicArgumentsBean.Builder(
                        commandLine.getOptionValue(USERNAME_OPTION.getOpt()),
                        commandLine.getOptionValue(HOST_OPTION.getOpt()));

                builder.withGroup(getGroupOption(commandLine));
                builder.withRole(getRoleOption(commandLine));
                builder.withPassword(getPasswordOption(commandLine));

                final ArgumentsBean.Builder argumentsBuilder = new ArgumentsBean.Builder(
                        commandLine.getOptionValue(ITEM_ID_OPTION.getOpt()));

                return argumentsBuilder.build(builder);
            } else {
                optionsHelpFormatter.printHelp(JAVA_JAR_ITEM_INFO, options);
            }

        } catch (ParseException e) {
            LOGGER.error("Parse error", e);
            optionsHelpFormatter.printHelp(JAVA_JAR_ITEM_INFO, options);
        }

        return null;
    }

    private String getGroupOption(final CommandLine commandLine) {
        final String group;
        if (commandLine.hasOption(GROUP_OPTION.getOpt())) {
            group = commandLine.getOptionValue(GROUP_OPTION.getOpt());
        } else {
            group = "";
        }
        return group;
    }

    private String getRoleOption(final CommandLine commandLine) {
        final String role;
        if (commandLine.hasOption(ROLE_OPTION.getOpt())) {
            role = commandLine.getOptionValue(ROLE_OPTION.getOpt());
        } else {
            role = "";
        }
        return role;
    }

    private String getPasswordOption(final CommandLine commandLine) {
        final String value;
        if (commandLine.hasOption(PASSWORD_OPTION.getOpt())) {
            value = commandLine.getOptionValue(PASSWORD_OPTION.getOpt());
        } else {
            value = "";
        }
        return value;
    }

    private boolean hasAllMandatoryOptions(final CommandLine commandLine) {
        boolean allOptions = true;
        for (final Option option : MANDATORY_OPTIONS) {
            allOptions &= commandLine.hasOption(option.getOpt());
        }

        return allOptions;
    }
}
