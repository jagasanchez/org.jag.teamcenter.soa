/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.client;

import org.jag.teamcenter.client.model.Arguments;
import org.jag.teamcenter.client.services.ArgumentsService;
import org.jag.teamcenter.soa.iteminfo.model.ItemInfo;
import org.jag.teamcenter.soa.iteminfo.service.ItemInfoService;
import org.jag.teamcenter.soa.model.ConnectionConfiguration;
import org.jag.teamcenter.soa.model.Credentials;
import org.jag.teamcenter.soa.services.ConnectionConfigurationFactory;
import org.jag.teamcenter.soa.services.ConnectionConnector;
import org.jag.teamcenter.soa.services.ConnectorModule;
import org.jag.teamcenter.soa.services.ServicesModule;
import org.jag.teamcenter.soa.services.SessionLoginException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * @author José A. García Sánchez
 */
public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    private final ConnectionConfigurationFactory connectionConfigurationFactory;
    private final ConnectionConnector connectionConnector;
    private final Arguments arguments;
    private final ItemInfoService itemInfoService;

    /**
     * @param args
     *            Collection of arguments given to the program
     */
    public static void main(final String[] args) {
        final Injector injector = Guice.createInjector(new ServicesModule(), new ConnectorModule());
        final ConnectionConfigurationFactory connectionConfigurationFactory = injector
                .getInstance(ConnectionConfigurationFactory.class);
        final ConnectionConnector connectionConnector = injector.getInstance(ConnectionConnector.class);
        final ArgumentsService argumentsService = injector.getInstance(ArgumentsService.class);
        final ItemInfoService itemInfoService = injector.getInstance(ItemInfoService.class);

        final Arguments arguments = argumentsService.parse(args);
        if (arguments == null) {
            return;
        }

        new Main(arguments, connectionConfigurationFactory, connectionConnector, itemInfoService).run();
    }

    private Main(final Arguments arguments, final ConnectionConfigurationFactory connectionConfigurationFactory,
            final ConnectionConnector connectionConnector, final ItemInfoService itemInfoService) {
        this.arguments = arguments;
        this.connectionConfigurationFactory = connectionConfigurationFactory;
        this.connectionConnector = connectionConnector;
        this.itemInfoService = itemInfoService;
    }

    private void run() {
        final ConnectionConfiguration connectionConfiguration = connectionConfigurationFactory
                .createConnectionConfiguration(arguments.getHost(), "discriminator");

        final Credentials credentials = new Credentials() {

            @Override
            public String getGroup() {
                return arguments.getGroup();
            }

            @Override
            public String getPassword() {
                return arguments.getPassword();
            }

            @Override
            public String getRole() {
                return arguments.getRole();
            }

            @Override
            public String getUsername() {
                return arguments.getUsername();
            }
        };

        connectionConnector.connect(connectionConfiguration, credentials);
        try {
            connectionConnector.login();
            final ItemInfo item = itemInfoService.find(arguments.getItemId());
            LOGGER.info("item: {}", item);
            connectionConnector.logout();
        } catch (SessionLoginException e) {
            LOGGER.error("Error when login with credentials [{}]", e.getCredentials());
        }
    }
}
