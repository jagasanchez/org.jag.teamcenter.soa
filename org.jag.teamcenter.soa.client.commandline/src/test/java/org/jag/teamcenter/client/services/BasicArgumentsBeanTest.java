/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.client.services;

import static com.google.common.truth.Truth.assertThat;

import org.jag.teamcenter.client.model.BasicArguments;
import org.junit.Before;
import org.junit.Test;

/**
 * @author José A. García Sánchez
 */
public class BasicArgumentsBeanTest {
    private BasicArgumentsBean.Builder underTestBuilder;

    @Before
    public void setUp() throws Exception {
        underTestBuilder = new BasicArgumentsBean.Builder("username", "host");
    }

    @Test
    public void createMinimalBean() {
        final BasicArguments basicArguments = underTestBuilder.build();

        assertThat(basicArguments).isNotNull();
        assertThat(basicArguments.getUsername()).isEqualTo("username");
        assertThat(basicArguments.getPassword()).isEqualTo("");
        assertThat(basicArguments.getGroup()).isEqualTo("");
        assertThat(basicArguments.getRole()).isEqualTo("");
        assertThat(basicArguments.getHost()).isEqualTo("host");
    }

    @Test
    public void createFullBean() {
        final BasicArguments basicArguments = underTestBuilder.withPassword("password").withGroup("group")
                .withRole("role").build();

        assertThat(basicArguments).isNotNull();
        assertThat(basicArguments.getUsername()).isEqualTo("username");
        assertThat(basicArguments.getPassword()).isEqualTo("password");
        assertThat(basicArguments.getGroup()).isEqualTo("group");
        assertThat(basicArguments.getRole()).isEqualTo("role");
        assertThat(basicArguments.getHost()).isEqualTo("host");
    }
}
