/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.client.services;

import static com.google.common.truth.Truth.assertThat;

import org.jag.teamcenter.client.model.Arguments;
import org.junit.Before;
import org.junit.Test;

/**
 * @author José A. García Sánchez
 */
public class ArgumentsBeanTest {
    private ArgumentsBean.Builder underTestBuilder;

    @Before
    public void setUp() throws Exception {
        underTestBuilder = new ArgumentsBean.Builder("itemId");
    }

    @Test
    public void createMinimalBean() {
        final BasicArgumentsBean.Builder basicArgumentsBuilder = new BasicArgumentsBean.Builder("username", "host");
        final Arguments arguments = underTestBuilder.build(basicArgumentsBuilder);

        assertThat(arguments).isNotNull();
        assertThat(arguments.getUsername()).isEqualTo("username");
        assertThat(arguments.getPassword()).isEqualTo("");
        assertThat(arguments.getGroup()).isEqualTo("");
        assertThat(arguments.getRole()).isEqualTo("");
        assertThat(arguments.getHost()).isEqualTo("host");
        assertThat(arguments.getItemId()).isEqualTo("itemId");
    }

    @Test
    public void createFullBean() {
        final BasicArgumentsBean.Builder basicArgumentsBuilder = new BasicArgumentsBean.Builder("username", "host")
                .withPassword("password").withGroup("group").withRole("role");
        final Arguments arguments = underTestBuilder.build(basicArgumentsBuilder);

        assertThat(arguments).isNotNull();
        assertThat(arguments.getUsername()).isEqualTo("username");
        assertThat(arguments.getPassword()).isEqualTo("password");
        assertThat(arguments.getGroup()).isEqualTo("group");
        assertThat(arguments.getRole()).isEqualTo("role");
        assertThat(arguments.getHost()).isEqualTo("host");
        assertThat(arguments.getItemId()).isEqualTo("itemId");
    }
}
