/*
 * MIT License
 *
 * Copyright (c) 2018 José A. García Sánchez
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.jag.teamcenter.client.services;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;

import org.apache.commons.cli.Options;
import org.jag.teamcenter.client.model.Arguments;
import org.jag.teamcenter.client.model.BasicArguments;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * @author José A. García Sánchez
 */
public class ArgumentsServiceBeanTest {
    private ArgumentsServiceBean underTest;

    @Mock
    private OptionsHelpFormatter optionsHelpFormatter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        underTest = new ArgumentsServiceBean(optionsHelpFormatter);
    }

    @Test
    public void parseMandatoryArguments() {
        final Arguments arguments = underTest.parse(
                new String[] { "-host", "http://localhost:8080/tc", "--username", "myUserId", "-id", "000245" });

        assertThat(arguments).isNotNull();
        assertThat(arguments.getHost()).isEqualTo("http://localhost:8080/tc");
        assertThat(arguments.getUsername()).isEqualTo("myUserId");
        assertThat(arguments.getGroup()).isEqualTo("");
        assertThat(arguments.getRole()).isEqualTo("");
        assertThat(arguments.getItemId()).isEqualTo("000245");
    }

    @Test
    public void parseMandatoryAndOptionalArguments() {
        final Arguments arguments = underTest
                .parse(new String[] { "-host", "http://localhost:8080/tc", "--username",
                        "myUserId", "--password", "myPassword", "--group", "Engineering", "--role", "Designer",
                        "--itemId", "000246" });

        assertThat(arguments).isNotNull();
        assertThat(arguments.getHost()).isEqualTo("http://localhost:8080/tc");
        assertThat(arguments.getUsername()).isEqualTo("myUserId");
        assertThat(arguments.getPassword()).isEqualTo("myPassword");
        assertThat(arguments.getGroup()).isEqualTo("Engineering");
        assertThat(arguments.getRole()).isEqualTo("Designer");
        assertThat(arguments.getItemId()).isEqualTo("000246");
    }

    @Test
    public void parsePartialArgumentsHost() {
        final BasicArguments arguments = underTest.parse(new String[] { "-host", "http://localhost:8080/tc" });

        assertThat(arguments).isNull();
        verify(optionsHelpFormatter).printHelp(Mockito.eq("java -jar itemInfo"), Mockito.any(Options.class));
    }

    @Test
    public void parsePartialArgumentsItemId() {
        final BasicArguments arguments = underTest.parse(new String[] { "--itemId", "000016" });

        assertThat(arguments).isNull();
        verify(optionsHelpFormatter).printHelp(Mockito.eq("java -jar itemInfo"), Mockito.any(Options.class));
    }

    @Test
    public void parseArgumentsWithoutValue() {
        final BasicArguments arguments = underTest.parse(new String[] { "-host", "--itemId", "000016" });

        assertThat(arguments).isNull();
        verify(optionsHelpFormatter).printHelp(Mockito.eq("java -jar itemInfo"), Mockito.any(Options.class));
    }

    @Test
    public void parseArgumentsWithHelp() {
        final BasicArguments arguments = underTest.parse(new String[] { "--help", "--itemId", "000016" });

        assertThat(arguments).isNull();

        verify(optionsHelpFormatter).printHelp(Mockito.eq("java -jar itemInfo"), Mockito.any(Options.class));
    }
}
